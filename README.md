# README #

Il s'agit d'un jeu orienté mobile et jouable à 2. Les joueurs retirent chacun leur tour une, deux ou trois "allumette(s)".
Le but est de ne pas retirer la dernière allumette

![alumettes.png](https://bitbucket.org/repo/RnKbBr/images/864500883-alumettes.png)