
var load_state = {

  preload: function() {
    GAME.load.image("background", "img/background.png");
    GAME.load.image("bouton", "img/bouton.png");
    GAME.load.image("alumette", "img/alumette.png");
    GAME.load.image("titre", "img/titre.png");
    GAME.load.image("jeton", "img/jeton.png");
    GAME.load.image("winner", "img/winner.png");
    GAME.load.image("loser", "img/loser.png");
    GAME.load.image("retry", "img/retry.png");
    GAME.load.image("un", "img/un.png");
    GAME.load.image("deux", "img/deux.png");
    GAME.load.image("trois", "img/trois.png");
  },


  create: function() {

    GAME.state.start("menu");
  }

};
