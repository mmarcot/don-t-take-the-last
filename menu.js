
var menu_state = {

  create: function() {

    // dimensionnement EXACT FIT :
    GAME.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
    GAME.scale.setExactFit();
    GAME.scale.refresh();

    GAME.add.sprite(0,0,"background");
    GAME.input.onDown.add(this.start, this);

    // affichage du titre :
    var titre = GAME.add.sprite(GAME.world.width/2, GAME.world.height/3, "titre");
    titre.width = GAME.world.width-20;
    titre.anchor.setTo(0.5,0.5);

    // affichage d'un texte :
    var text = GAME.add.text(GAME.world.width/2, GAME.world.height/1.5,
      "Tap to start", {font: "40px Arial", fill: "#000"});
    text.anchor.setTo(0.5,0.5);
  },


  start: function() {
    GAME.state.start("play");
  }

};
