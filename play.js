
var play_state = {

  create: function() {
    GAME.stage.backgroundColor = "#C6EAFF";

    // on créé les alumettes :
    this.alumettes = GAME.add.group();
    this.createAlumettes();

    // on crée et ajoute les boutons :
    this.buttons = GAME.add.group();
    this.createButtons();
    for(var i=0; i<this.buttons.length; i++) {
      this.buttons.getAt(i).inputEnabled = true;
      this.buttons.getAt(i).events.onInputDown.add(this.removeAlumettes, this);
    }

    // on crée le jeton
    this.jeton = GAME.add.sprite(GAME.world.width, GAME.world.centerY, "jeton");
    this.jeton.anchor.setTo(1,0);
    this.placerJeton();

    // affichage du score :
    this.lab_score_bas = GAME.add.text(25, GAME.world.height, ""+SCORE_BAS, {font: "35px Arial", fill: "#000"});
    this.lab_score_bas.anchor.setTo(0.5,1);
    this.lab_score_haut = GAME.add.text(25, 0, ""+SCORE_HAUT, {font: "35px Arial", fill: "#000"});
    this.lab_score_haut.anchor.setTo(0.5,1);
    this.lab_score_haut.angle += 180;

    this.partie_terminee = false;
    TOUR = Math.floor(Math.random()*2+1);
  },


  update: function() {
    if(this.alumettes.length < 1 && !this.partie_terminee) {
      this.partie_terminee = true;
      this.displayWinner();
    }
    else if(this.partie_terminee) {
      this.retry.angle += 1;
    }
    else {
      this.placerJeton();
    }
  },


  createAlumettes: function() {
    for(var i=0; i<NB_ALUMETTES; i++) {
      this.alumettes.create(i*20+150, GAME.world.height/2, "alumette").anchor.setTo(0,0.5);
    }
  },


  createButtons: function() {
    var i, button;

    this.buttons_bot = [];
    this.buttons_top = [];

    var numbers = ["un", "deux", "trois"];

    // boutons du bas :
    for(i=0; i<3; i++) {
      var nb;

      this.buttons_bot[i] = this.buttons.create(i*190+50, GAME.world.height, "bouton");
      this.buttons_bot[i].anchor.setTo(0,1);
      this.buttons_bot[i].scale.setTo(1.5);

      nb = GAME.add.sprite(i*190+85, GAME.world.height, numbers[i]);
      nb.anchor.setTo(0,1);
      nb.scale.setTo(0.8);
    }

    // boutons du haut :
    for(i=0; i<3; i++) {
      var nb;

      this.buttons_top[i] = this.buttons.create(i*190+50, 0, "bouton");
      this.buttons_top[i].anchor.setTo(0,0);
      this.buttons_top[i].scale.setTo(1.5);

      nb = GAME.add.sprite(i*190+85, 0, numbers[2-i]);
      nb.anchor.setTo(1,1);
      nb.scale.setTo(0.8);
      nb.angle += 180;
    }
  },


  placerJeton: function() {
    if(TOUR === 1) { // jeton en bas :
      GAME.add.tween(this.jeton).to({y:GAME.world.height-50}, 100).start();
    }
    else
      GAME.add.tween(this.jeton).to({y:0}, 100).start();
  },


  removeAlumettes: function() {
    if(!this.partie_terminee) {
      var third_width = GAME.world.width/3;
      var middle_height = GAME.world.height/2;
      var nb = 0;

      if(GAME.input.activePointer.y > GAME.world.centerY) { // joueur bas :
        if(GAME.input.activePointer.x < third_width)
          nb = 1;
        else if(GAME.input.activePointer.x < 2* third_width)
          nb = 2;
        else nb = 3;
      }
      else { // joueur haut :
        if(GAME.input.activePointer.x < third_width)
          nb = 3;
        else if(GAME.input.activePointer.x < 2* third_width)
          nb = 2;
        else nb = 1;
      }

      console.log("retirer " + nb + " alumettes");

      if( (TOUR === 1 && GAME.input.activePointer.y > middle_height) ||
          (TOUR === 2 && GAME.input.activePointer.y < middle_height)) {
        this.changeTour();
        for(var i=0; i<nb; i++) {
          this.alumettes.getAt(this.alumettes.length-1).destroy();
        }
      }
    }
  },

  // 1 = bas
  // 2 = haut
  changeTour: function() {
    if(TOUR === 1)
      TOUR = 2;
    else TOUR = 1;
  },


  displayWinner: function() {
    var winner = TOUR === 1 ? 1 : 2;
    var img_bas, img_haut;
    if(winner === 1) {
      img_bas = "winner";
      img_haut = "loser";
      SCORE_BAS += 1;
      this.lab_score_bas.text = "" + SCORE_BAS;
    }
    else {
      img_bas = "loser";
      img_haut = "winner";
      SCORE_HAUT += 1;
      this.lab_score_haut.text = "" + SCORE_HAUT;

    }

    var spr_haut = GAME.add.sprite(GAME.world.centerX, 0, img_haut);
    spr_haut.anchor.setTo(0.5,1);
    spr_haut.angle = 180;
    GAME.add.sprite(GAME.world.centerX, GAME.world.height-75, img_bas).anchor.setTo(0.5,0);


    this.retry = GAME.add.sprite(GAME.world.centerX, GAME.world.centerY, "retry");
    this.retry.scale.setTo(0.65);
    this.retry.anchor.setTo(0.5,0.5);
    this.retry.inputEnabled = true;
    this.retry.events.onInputDown.add(this.restart, this);

    GAME.add.tween(this.jeton).to({y:GAME.world.height/2-25}, 100).start();
  },


  restart: function() {
    GAME.state.restart();
  }


};
